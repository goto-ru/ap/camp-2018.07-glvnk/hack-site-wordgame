import * as PIXI from "pixi.js";
// https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Classes
export class Player {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.r = 10;
        this.speed = 2;
    }


    draw() {
        // http://pixijs.download/dev/docs/PIXI.Graphics.html
        const graphics = new PIXI.Graphics();
        graphics.beginFill(0x8B0000);
        graphics.drawCircle(this.x, this.y, this.r);
        graphics.endFill();

        return graphics
    }

    go_left = () => {
        this.x -= this.speed
    };

    go_right = () => {
        this.x += this.speed
    };

    go_up = () => {
        this.y -= this.speed
    };

    go_down = () => {
        this.y += this.speed
    };
}