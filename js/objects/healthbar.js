import * as PIXI from "pixi.js";
// https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Classes
export class Healthbar {
    constructor() {
        this.result = '';
        this.x = 3000;
        this.y = 3000;
    };

    gameover =(result) => {
        this.result = result;
        this.y = 250;
        this.x = 600;
    };

    draw() {
        // http://pixijs.download/dev/docs/PIXI.Graphics.html
            this.text = ` ${this.result}`;
            const MobText = new PIXI.Text(this.text, {fill: 0x008000, fontSize: 28});
            MobText.x = this.x;
            MobText.y = this.y;
            return MobText;
    }
}