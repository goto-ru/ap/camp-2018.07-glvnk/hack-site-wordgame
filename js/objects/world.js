import {Info} from "./info"
import {Mob} from './mobs'
import {Healthbar} from './healthbar'

export class World {
    constructor() {
        // Создадим игрока
        this.healthbar = new Healthbar();
        this.mobs = [];
        this.schet = 0;
        this.schet2 = 0;
        this.click_x = 0;
        this.click_y = 0;
        this.front = 0;
        this.ad = 0;
        this.rasp = 0;
        this.met = 0;
        this.kmb1 = 0;

    }

    click = (x, y) => {
        console.log(x,y);
        this.click_x = x;
        this.click_y = y;
        this.mobs.forEach((m) => {
            if (((m.x + 75 > this.click_x) && (m.x - 75 < this.click_x)) && ((m.y + 75 > this.click_y)  && (m.y - 75 < this.click_y))) {
                console.log("clicked on ", m.text);
                if (m.text == 'Ява скрыпт' || m.text == 'Риактъ') {
                    this.front += 1
                } else if (m.text == 'Дата синс' || m.text == 'Восстание машин') {
                    this.ad += 1
                } else if (m.text == 'Линукс' || m.text == 'Биток') {
                    this.rasp += 1
                } else if (m.text == 'МЕТАЛЛ' || m.text == 'Жалюзи') {
                    this.met += 1
                } else if (m.text == 'Ведро' || m.text == 'Свинья') {
                    this.kmb1 += 1
                }
            }
        });
    };

    // В зависисмости от нажатых клавиш изменяем среду
    get_items() {
        this.schet2 += 1;
        if (this.schet2 > 1000) {
            console.log([this.front, this.kmb1, this.ad, this.met, this.rasp]);

            if (this.front >= this.ad && this.front >= this.rasp && this.front >= this.met && this.front >= this.kmb1) {
                this.result = 'ВЫ НА ФРОНТЕ'
            } else if (this.ad >= this.front && this.ad >= this.rasp && this.ad >= this.met && this.ad >= this.kmb1) {
                this.result = 'ВЫ В АДУ'
            } else if (this.rasp >= this.front && this.rasp >= this.ad && this.rasp >= this.met && this.rasp >= this.kmb1) {
                this.result = 'ВЫ РАСПРЕДЕЛИТЕЛЬНЫ'
            } else if (this.kmb1 >= this.front && this.kmb1 >= this.rasp && this.kmb1 >= this.met && this.kmb1 >= this.ad) {
                this.result = 'ВАМ К РОСТУ (КМБ1)'
            } else if (this.met >= this.front && this.met >= this.rasp && this.met >= this.kmb1 && this.met >= this.ad){
                this.result = 'ВАМ НА ЖЕЛЕЗО'
            }
            this.healthbar.gameover(this.result)
        }

        this.schet += 1;
        if (this.schet > 100) {
            this.mobs.push(new Mob('text'));
            this.schet = 0;
        }
        this.mobs.forEach((m) => {
            m.lol()
        });
        this.mobs = this.mobs.filter((Mob) => (((Mob.x + 75 < this.click_x) || (Mob.x - 75 > this.click_x)) || ((Mob.y + 75 < this.click_y) || (Mob.y - 75 > this.click_y))));
        this.click_x = 3000;
        this.click_y = 3000;
        this.mobs = this.mobs.filter((Mob) => (Mob.y  < 600));
        return [this.healthbar, ...this.mobs]
    }
}

