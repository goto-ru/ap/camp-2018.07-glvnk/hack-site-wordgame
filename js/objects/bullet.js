import * as PIXI from "pixi.js";
// https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Classes
export class Bullet {
    constructor(x, y, Tx, Ty) {
        this.x = x;
        this.y = y;
        this.r = 6;
        this.Tx = Tx;
        this.Ty = Ty;
        this.k = (this.Ty - this.y)/(this.Tx-this.x);
        this.b = this.y - (this.Ty-this.y)/(this.Tx-this.x)*this.x;
        this.speed = 1;
        if (this.x > this.Tx) {
            this.speed *= -1
        }
    }

    tick = () => {
        this.x += this.speed;
        this.y = this.x * this.k + this.b;
    };

    draw() {
        this.tick();
        // http://pixijs.download/dev/docs/PIXI.Graphics.html
        const graphics = new PIXI.Graphics();

        graphics.beginFill(0x8B0000);
        graphics.drawCircle(this.x, this.y, this.r);
        graphics.endFill();

        return graphics
    }
}
